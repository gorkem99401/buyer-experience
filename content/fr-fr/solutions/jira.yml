---
  title: GitLab avec Jira
  description: Automatisez votre travail depuis GitLab dans Jira
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab avec Jira
        subtitle: Automatisez votre travail depuis GitLab dans Jira
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: /learn/#agile_management
          text: Apprendre de nouvelles compétences
          data_ga_name: start learning
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image: GitLab avec Jira"
    - name: featured-media
      data:
        column_size: 4
        media:
          - title: Intégration GitLab <-> Jira
            aos_animation: fade-up
            aos_duration: 500
            text: |
              Une fois que votre projet GitLab est intégré à votre instance Jira, vous pouvez automatiquement détecter et croiser les activités entre le projet GitLab et votre projet dans Jira, quel que soit ce projet.
            icon:
              name: kanban
              alt: Kanban
              variant: marketing
          - title: Intégration du panneau Jira Dev
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              En complément de notre intégration des projets dans Jira, vous pouvez désormais intégrer vos projets GitLab au panneau de développement de tickets Jira.
            icon:
              name: computer-test
              alt: Intégration du panneau Jira Dev
              variant: marketing
          - title: Migrer de Jira vers GitLab
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              GitLab offre un outil de gestion de projet robuste qui regroupe votre planification agile dans une plateforme unique qui héberge votre SCM, CI/CD, la sécurité et bien plus encore !
            icon:
              name: scale
              alt: Intégration du panneau Jira Dev
              variant: marketing
    - name: featured-media
      data:
        header: Fonctionnement de l'intégration GitLab-Jira
        column_size: 4
        media:
          - title: Intégration de base GitLab <-> Jira
            aos_animation: zoom-in-up
            aos_duration: 500
            text: |
              * Mentionnez l'ID d'un ticket Jira dans un message de validation ou une MR (requête de fusion)
               
              * Mentionnez qu'une validation ou une MR résout ou clôture un ticket Jira spécifique
               
              * Affichez les tickets Jira directement dans GitLab
            image:
              url: /nuxt-images/enterprise/source-code.jpg
              alt: "GitLab <-> Jira"
          - title: Intégration du panneau Jira Dev
            aos_animation: zoom-in-up
            aos_duration: 1000
            text: |
              * Accédez facilement aux requêtes de fusion, branches et validations GitLab associées directement à partir d'un ticket Jira
               
              * Fonctionne avec GitLab Auto-géré ou GitLab.com intégré à Jira hébergé par votre Jira Cloud
               
              * Connectez tous les projets GitLab au sein d'un groupe principal ou d'un espace de nommage personnel à des projets dans l'instance Jira
            image:
              url: /nuxt-images/enterprise/ci-cd.jpg
              alt: "Intégration du panneau"
          - title: Migrer de Jira vers GitLab
            aos_animation: zoom-in-up
            aos_duration: 1500
            text: |
              * Importez vos tickets Jira dans GitLab.com ou dans votre instance GitLab Auto-géré
               
              * Importez directement le titre, la description et les labels
               
              * Mappez les utilisateurs Jira aux membres du projet GitLab
            image:
              url: /nuxt-images/enterprise/agile2.jpg
              alt: "Migration de GitLab vers Jira"
    - name: featured-media
      data:
        header: Vidéos explicatives
        text: |
          Voici une liste de ressources à propos de l'intégration GitLab-Jira que nous avons créées et qui vous seront particulièrement utiles pour comprendre la mise en œuvre de cette intégration.
        column_size: 4
        media:
          - title: Intégration de base GitLab-Jira
            aos_animation: fade-up
            aos_duration: 500
            text: |
              Bien qu'il vous soit toujours possible de migrer le contenu et le processus des tickets Jira vers le système de tickets GitLab, vous pouvez également choisir de continuer à utiliser Jira et de l'utiliser conjointement avec GitLab.
            link:
              text: Regarder
              href: https://docs.gitlab.com/ee/integration/jira/
              data_ga_name: Gitlab-Jira integration
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/fWvwkx5_00E?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: Panneau de développement GitLab-Jira
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              En complément de notre intégration des projets dans Jira, vous pouvez également intégrer vos projets GitLab au panneau de développement de tickets Jira.
            link:
              text: Regarder
              href: https://docs.gitlab.com/ee/integration/jira/
              data_ga_name: Gitlab-Jira development panel
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/VjVTOmMl85M?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: Importation des tickets de votre projet Jira dans GitLab
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              À l'aide de l'importateur Jira pour GitLab, vous pouvez importer vos tickets Jira dans GitLab.com ou dans votre instance GitLab Auto-géré.
            link:
              text: Regarder
              href: https://docs.gitlab.com/ee/user/project/import/jira/
              data_ga_name: Import jira issue to GitLab
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/cixs4NJB3H8?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: Consulter la liste des tickets Jira dans GitLab
            aos_animation: fade-up
            aos_duration: 500
            text: |
              Pour les organisations qui utilisent Jira comme principal outil de suivi de projets, il peut être difficile pour les contributeurs de travailler sur plusieurs systèmes et de maintenir une source unique de vérité.
            link:
              text: Regarder
              href: https://docs.gitlab.com/ee/user/project/import/jira/
              data_ga_name: View jira issue list in GitLab
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/_yDcD_jzSjs?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: Mappage des utilisateurs Jira avec les utilisateurs GitLab lors de l'importation des tickets
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              Lors de l'importation de tickets Jira dans GitLab, vous pouvez désormais mapper les utilisateurs Jira avec les membres du projet GitLab avant de lancer l'importation. Cela permet à l'importateur de définir le rapporteur et la personne assignée adéquats pour les tickets que vous déplacez dans GitLab.
            link:
              text: Regarder
              href: https://docs.gitlab.com/ee/user/project/import/jira/
              data_ga_name: Map jira users to GitLab
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/cixs4NJB3H8?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: Feuille de route de gestion de projet GitLab
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              Nous travaillons constamment à l'amélioration de l'intégration de GitLab avec Jira. Nous vous invitons donc à nous faire part de vos commentaires ou à découvrir les nouveautés des prochaines releases.
            link:
              text: Regarder
              href: https://gitlab.com/groups/gitlab-org/-/epics/2738/
              data_ga_name: GitLab project management roadmap
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/bT60rJEoWhw?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: logo-links
      data:
        header: Partenaires Open Source
        column_size: 2
        logos:
          - logo_url: /nuxt-images/enterprise/logo-dish.svg
            logo_alt: Dish
            aos_animation: zoom-in
            aos_duration: 200
          - logo_url: /nuxt-images/enterprise/logo-expedia.svg
            logo_alt: Expedia
            aos_animation: zoom-in
            aos_duration: 400
          - logo_url: /nuxt-images/enterprise/logo-goldman-sachs.svg
            logo_alt: Goldman Sachs
            aos_animation: zoom-in
            aos_duration: 600
          - logo_url: /nuxt-images/enterprise/logo-nasdaq.svg
            logo_alt: Nasdaq
            aos_animation: zoom-in
            aos_duration: 800
          - logo_url: /nuxt-images/enterprise/logo-uber.svg
            logo_alt: Uber
            aos_animation: zoom-in
            aos_duration: 1000
          - logo_url: /nuxt-images/enterprise/logo-verizon.svg
            logo_alt: Verizon
            aos_animation: zoom-in
            aos_duration: 1200
