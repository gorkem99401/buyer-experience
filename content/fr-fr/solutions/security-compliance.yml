---
  title: Intégration de la sécurité dans votre cycle de vie DevOps
  description: Tests de sécurité des applications GitLab pour SAST, DAST, analyse des dépendances, analyse des conteneurs et autres dans le pipeline DevSecOps CI avec gestion des vulnérabilités et conformité.
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          -  Sécurité et conformité en amont
        title: Sécurité et gouvernance de GitLab
        subtitle: GitLab permet à vos équipes de concilier rapidité et sécurité en automatisant la livraison de logiciels et en sécurisant votre chaîne d'approvisionnement logicielle de bout en bout.
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Essayer Ultimate gratuitement
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Des questions ? Contactez-nous
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/gitlab-security-and-governance/gitlab-security-and-governance.jpg
          image_url_mobile: /nuxt-images/gitlab-security-and-governance/gitlab-security-and-governance.jpg
          alt: "Image\_: GitLab pour le secteur public"
          bordered: vrai
    - name: 'by-industry-intro'
      data:
        intro_text: Adoptée par
        logos:
          - name: Logo UBS
            image: "/nuxt-images/home/logo_ubs_mono.svg"
            url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
            aria_label: Lien vers l'étude de cas d'UBS
          - name: Logo Hackerone
            image: /nuxt-images/logos/hackerone-logo.png
            url: https://about.gitlab.com/customers/hackerone/
            aria_label: Lien vers l'étude de cas Hackerone
          - name: Logo The Zebra
            image: /nuxt-images/logos/zebra.svg
            url: https://about.gitlab.com/customers/thezebra/
            aria_label: Lien vers l'étude de cas The Zebra
          - name: Logo Hilti 
            image: /nuxt-images/logos/hilti_logo.svg
            url: https://about.gitlab.com/customers/hilti/
            aria_label: Lien vers l'étude de cas Hilti
          - name: Logo Conversica
            image: /nuxt-images/logos/conversica.svg
            url: https://about.gitlab.com/customers/conversica/
            aria_label: Lien vers l'étude de cas Conversica
          - name: Logo Bendigo and Adelaide Bank
            image: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
            url: https://about.gitlab.com/customers/bab/
            aria_label: Lien vers l'étude de cas Bendigo and Adelaide Bank
          - name: Logo Glympse
            image: /nuxt-images/logos/glympse-logo-mono.svg
            url: https://about.gitlab.com/customers/glympse/
            aria_label: Lien vers l'étude de cas Glympse
    - name: 'side-navigation-variant'
      links:
        - title: Présentation
          href: '#overview'
        - title: Avantages
          href: '#benefits'
        - title: Capacités
          href: '#capabilities'
        - title: Clients
          href: '#customers'
        - title: Tarifs
          href: '#pricing'
        - title: Ressources
          href: '#resources'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: vrai
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Livraison logicielle rapide et sécurisée
                is_accordion: false
                items:
                  - icon:
                      name: continuous-integration
                      alt: Icône d'intégration continue
                      variant: marketing
                    header: Sécurité intégrée
                    text: Une seule plateforme, un seul prix, tout est prêt à l'emploi.
                    link_text: En savoir plus sur la plateforme DevSecOps
                    link_url: /platform
                    ga_name: platform
                    ga_location: overview
                  - icon:
                      name: devsecops
                      alt: Icône DevSecOps
                      variant: marketing
                    header: Sécurité continue
                    text: Analyses automatisées avant et après l'introduction du code.
                    link_text: Pourquoi utiliser GitLab
                    link_url: /why-gitlab/
                    ga_name: why gitlab
                    ga_location: overview
                  - icon:
                      name: release
                      alt: Icône de bouclier avec une coche
                      variant: marketing
                    header: Contrôle complet
                    text: Mettez en place des garde-fous et automatisez les politiques.
                    link_text: En savoir plus sur notre approche de la plateforme
                    ga_name: devops platform
                    ga_location: overview
                    link_url: /solutions/devops-platform/
            - name: 'solutions-video-feature'
              data:
                video:
                  url: 'https://player.vimeo.com/video/762685637?h=f96e969756'
        - name: 'div'
          id: 'benefits'
          slot_enabled: vrai
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: Sécurité. Conformité. Contrôles en amont.
                cards:
                  - title: Sécurisez votre chaîne d'approvisionnement logicielle
                    description: GitLab vous aide à sécuriser votre chaîne d'approvisionnement logicielle de bout en bout (y compris les sources, la compilation, les dépendances et les artefacts publiés), à créer un inventaire des logiciels utilisés (nomenclature logicielle) et à appliquer les contrôles nécessaires.
                    data_ga_name: supply chain
                    data_ga_location: benefits
                    cta: En savoir plus
                    icon:
                      name: less-risk
                      alt: Icône de risque moindre
                      variant: marketing
                    href: /solutions/supply-chain/
                  - title: Gestion des vecteurs de menaces
                    description: GitLab vous aide à contrôler la sécurité en amont en analysant automatiquement les vulnérabilités dans le code source, les conteneurs, les dépendances et les applications en cours d'exécution. Des garde-fous peuvent être mis en place pour sécuriser votre environnement de production.
                    data_ga_name: docs app security
                    data_ga_location: benefits
                    cta: En savoir plus
                    icon:
                      name: devsecops
                      alt: Icône de DevSecOps
                      variant: marketing
                    href: https://docs.gitlab.com/ee/user/application_security/get-started-security.html
                  - title: Respect des exigences de conformité
                    description: GitLab peut vous aider à suivre vos modifications, à mettre en œuvre les contrôles nécessaires pour protéger ce qui est mis en production et à garantir le respect des licences et des cadriciels réglementaires.
                    data_ga_name: compliance
                    data_ga_location: benefits
                    cta: En savoir plus
                    icon:
                      name: eye-magnifying-glass
                      alt: Icône de loupe
                      variant: marketing
                    href: /solutions/compliance/
        - name: 'div'
          id: 'capabilities'
          slot_enabled: vrai
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                subtitle: Contrôle en amont de la sécurité
                sub_description: ''
                white_bg: vrai
                sub_image: /nuxt-images/solutions/dev-sec-ops/dev-sec-ops.png
                solutions:
                  - title: Intégrez les tests de sécurité dans le pipeline CI/CD
                    description: Utilisez nos scanners intégrés et intégrez des scanners personnalisés. Le contrôle en amont de la sécurité permet aux développeurs de trouver et de corriger les failles de sécurité au fur et à mesure qu'elles sont créées. Les scanners complets comprennent SAST, DAST, l'analyse des secrets, l'analyse des dépendances, l'analyse des conteneurs, l'analyse IaC, la sécurité de l'API et les tests à données aléatoires.
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/user/application_security/?_gl=1*hwzvlj*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2MzIyOTY1My44OS4xLjE2NjMyMzAyNTYuMC4wLjA.
                    data_ga_name: ci/cd
                    data_ga_location: solutions block
                  - title: Gestion des dépendances
                    description: Compte tenu de la multitude de composants open source utilisés dans le développement de logiciels, la gestion manuelle de ces dépendances est une tâche redoutable. Recherchez les failles de sécurité dans les dépendances des applications et des conteneurs et créez une nomenclature logicielle (SBOM) des dépendances utilisées.
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/user/application_security/dependency_list/
                    data_ga_name: dependencies management
                    data_ga_location: solutions block
                  - title: Gestion des vulnérabilités
                    description: Développez vos équipes de sécurité en faisant apparaître les vulnérabilités dans le flux de travail naturel des développeurs et en les résolvant avant de pousser le code en production. Les professionnels de la sécurité peuvent examiner, trier et gérer les vulnérabilités provenant de pipelines, d'analyses à la demande, de tierces parties et de primes aux bogues, le tout en un seul endroit.
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/user/application_security/vulnerability_report/
                    data_ga_name: vulnerability management
                    data_ga_location: solutions block
                  - title: Des applications sécurisées en cours d'exécution
                    description: Protégez vos charges de travail en configurant un tunnel CI/CD sécurisé avec vos clusters, en exécutant une analyse dynamique de la sécurité des applications, une analyse opérationnelle des conteneurs et en configurant une liste blanche d'adresses IP.
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/user/application_security/
                    data_ga_name: secure applications
                    data_ga_location: solutions block
                  - title: Mise en place de garde-fous et garantie de conformité
                    description: Automatisez les politiques de sécurité et de conformité tout au long du cycle de développement de vos logiciels. Les pipelines conformes garantissent que les politiques de pipeline ne sont pas contournées, tandis que les contrôles communs fournissent des garde-fous de bout en bout.
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html
                    data_ga_name: guardrails guardrails
                    data_ga_location: solutions block
        - name: 'div'
          id: 'customers'
          slot_enabled: vrai
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                align: gauche
                header: |
                  Approuvée par les entreprises.
                  <br />
                  Adorée par les développeurs.
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/hackerone-logo.png
                      alt: Logo Hackerone
                    quote: GitLab nous aide à détecter rapidement les failles de sécurité et la plateforme les intègre dans le flux de travail des développeurs. Les développeurs peuvent pousser du code dans GitLab CI et immédiatement recevoir des commentaires lors de l'une des nombreuses étapes d'audit en cascade. Ils peuvent ainsi constater la présence éventuelle d'une vulnérabilité de sécurité. De plus, les développeurs peuvent même créer de nouvelles étapes qu'ils jugent nécessaires pour tester des problèmes de sécurité très spécifiques.
                    author: Mitch Trale
                    position: Head of Infrastrusture, Hackerone
                    ga_carousel: hackerone testimonial
                    ga_location: customers
                    url: /customers/hackerone/
                  - title_img:
                      url: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
                      alt: Logo Bendigo and Adelaide Bank
                    quote: Nous disposons désormais d'une solution toujours innovante qui s'aligne sur notre objectif de transformation numérique.
                    author: Caio Trevisan
                    position: Head of Devops Enablement, Bendigo and Adelaide Bank
                    ga_carousel: bendigo and adelaide bank testimonial
                    ga_location: customers
                    url: /customers/bab/
                  - title_img:
                      url: /nuxt-images/logos/zebra.svg
                      alt: Logo The Zebra
                    quote: La plus grande valeur (de GitLab) est que cette plateforme permet à nos équipes de développement de jouer un rôle plus important dans le processus de déploiement. Auparavant, seules quelques personnes connaissaient vraiment le processus, mais maintenant, la quasi-totalité de l'organisation de développement sait comment fonctionne le pipeline CI, peut travailler avec, y ajouter de nouveaux services, et mettre les choses en production sans que l'infrastructure ne soit le goulet d'étranglement.
                    author: Dan Bereczki
                    position: Sr. Software Manager, The Zebra
                    ga_carousel: the zebra testimonial
                    ga_location: customers
                    url: /customers/thezebra/
                  - title_img:
                      url: /nuxt-images/logos/hilti_logo.svg
                      alt: Logo Hilti
                    quote: La solution GitLab est empaquetés comme une suite et livrés avec un installateur très sophistiqué. Et pourtant tout fonctionne. C'est très pratique si vous êtes une entreprise qui souhaite simplement le mettre en place et le rendre opérationnel.
                    author: Daniel Widerin
                    position: Head of Software Delivery, HILTI
                    ga_location: customers
                    ga_carousel: hilti testimonial
                    url: /customers/hilti/
        - name: div
          id: 'pricing'
          slot_enabled: vrai
          slot_content:
            - name: 'tier-block'
              class: 'slp-mt-96'
              id: 'tarifs'
              data:
                header: Quelle édition vous convient le mieux ?
                cta:
                  url: /pricing/
                  text: En savoir plus sur la tarification
                  data_ga_name: pricing learn more
                  data_ga_location: pricing
                  aria_label: pricing
                tiers:
                  - id: free
                    title: Forfait Gratuit
                    items:
                      - Tests statiques de sécurité des applications (SAST) et détection des secrets
                      - Résultats dans le fichier json
                    link:
                      href: /pricing/
                      text: En savoir plus
                      data_ga_name: free learn more
                      data_ga_location: pricing
                      aria_label: édition gratuite
                  - id: premium
                    title: Premium
                    items:
                      - Tests statiques de sécurité des applications (SAST) et détection des secrets
                      - Résultats dans le fichier json
                      - Approbations MR et contrôles plus courants
                    link:
                      href: /pricing/
                      text: En savoir plus
                      data_ga_name: premium learn more
                      data_ga_location: pricing
                      aria_label: édition premium
                  - id: ultimate
                    title: Ultimate
                    items:
                      - Tout ce qui compose l'édition Premium, plus
                      - Les scanners de sécurité complets incluent SAST, DAST, Secrets, dépendances, conteneurs, IaC, API, images de clusters et tests à données aléatoires
                      - Résultats exploitables dans le pipeline MR
                      - Pipelines de conformité
                      - Tableaux de bord de sécurité et de conformité
                      - Et bien plus encore
                    link:
                      href: /pricing/
                      text: En savoir plus
                      data_ga_name: ultimate learn more
                      data_ga_location: pricing
                      aria_label: édition ultimate
                    cta:
                      href: /free-trial/
                      text: Essayer Ultimate gratuitement
                      data_ga_name: try ultimate
                      data_ga_location: pricing
                      aria_label: édition ultimate
        - name: 'div'
          id: 'resources'
          slot_enabled: true
          slot_content:
          - name: solutions-resource-cards
            data:
              title: Ressources connexes
              align: left
              column_size: 4
              grouped: true
              cards:
                - icon:
                    name: video
                    variant: marketing
                    alt: Icône de vidéo
                  event_type: "Vidéo"
                  header: Contrôle en amont de la sécurité - présentation de la sécurité de GitLab
                  link_text: "Regarder maintenant"
                  image: "/nuxt-images/solutions/dev-sec-ops/shifting-security-left.jpeg"
                  href: https://www.youtube.com/embed/XnYstHObqlA?enablejsapi=1
                  data_ga_name: Shifting Security Left - GitLab Security Overview
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Icône de vidéo
                  event_type: "Vidéo"
                  header: Gestion des vulnérabilités et séparation des tâches avec GitLab
                  link_text: "Regarder maintenant"
                  image: /nuxt-images/solutions/dev-sec-ops/managing-vulnerabilities.jpeg
                  href: https://www.youtube.com/embed/J5Frv7FZtnI?enablejsapi=1
                  data_ga_name: Managing Vulnerabilities and Enabling Separation of Duties with GitLab
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Icône de vidéo
                  event_type: "Vidéo"
                  header: 'Release GitLab 15 : de nouvelles fonctionnalités de sécurité'
                  link_text: "Regarder maintenant"
                  image: "/nuxt-images/solutions/dev-sec-ops/gitlab-15-release.jpeg"
                  href: https://www.youtube.com/embed/BasGVNvOFGo?enablejsapi=1
                  data_ga_name: GitLab 15 Release - New Security Features
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Icône de vidéo
                  event_type: "Vidéo"
                  header: SBOM et attestation
                  link_text: "Regarder maintenant"
                  image: "/nuxt-images/solutions/compliance/sbom-and-attestation.jpeg"
                  href: https://www.youtube.com/embed/wX6aTZfpJv0?enablejsapi=1
                  data_ga_name: SBOM and Attestation
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: Icône d'un e-book
                  event_type: "Livre"
                  header: "Guide sur la sécurité de la chaîne d'approvisionnement logicielle"
                  link_text: "En savoir plus"
                  image: "/nuxt-images/resources/resources_11.jpeg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/360915/35d042c6-3449-4d50-b2e9-b08d9a68f7a1.pdf
                  data_ga_name: "Guide to software supply chain security"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: Icône d'un e-book
                  event_type: "Livre"
                  header: "Le DevSecOps avec GitLab CI/CD"
                  link_text: "En savoir plus"
                  image: "/nuxt-images/resources/resources_1.jpeg"
                  href: https://page.gitlab.com/achieve-devsecops-cicd-ebook.html
                  data_ga_name: "DevSecOps with GitLab CI/CD"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: Icône d'un e-book
                  event_type: "Livre"
                  header: "Enquête GitLab DevSecOps"
                  link_text: "En savoir plus"
                  image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
                  href: https://cdn.pathfactory.com/assets/10519/contents/432983/c6140cad-446b-4a6c-96b6-8524fac60f7d.pdf
                  data_ga_name: "GitLab DevSecOps survey"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Icône de blog
                  event_type: "Blog"
                  header: "9\_conseils pour un contrôle en amont"
                  link_text: "En savoir plus"
                  href: https://about.gitlab.com/blog/2020/06/23/efficient-devsecops-nine-tips-shift-left/
                  image: /nuxt-images/blogimages/hotjar.jpg
                  data_ga_name: "9 tips to shift left"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Icône de blog
                  event_type: "Blog"
                  header: "Sécuriser la chaîne d'approvisionnement logicielle grâce à l'attestation automatisée"
                  link_text: "En savoir plus"
                  href: https://about.gitlab.com/blog/2022/08/10/securing-the-software-supply-chain-through-automated-attestation/
                  image: /nuxt-images/blogimages/cover_image_regenhu.jpg
                  data_ga_name: "Securing the software supply chain through automated attestation"
                  data_ga_location: resource cards
                - icon:
                    name: report
                    alt: Icône de rapport
                    variant: marketing
                  event_type: "Rapport d'analyste"
                  header: "Avis de 451 Research\_: GitLab élargit la vision de DevOps"
                  link_text: "En savoir plus"
                  href: "https://page.gitlab.com/resources-report-451-gitlab-broadens-devops.html"
                  image: /nuxt-images/blogimages/hemmersbach_case_study.jpg
                  data_ga_name: "451 Research opinion: GitLab broadens view of DevOps"
                  data_ga_location: resource cards
                - icon:
                    name: report
                    alt: Icône de rapport
                    variant: marketing
                  event_type: "Rapport d'analyste"
                  header: "GitLab obtient la position de Challenger dans le rapport Gartner Magic Quadrant 2022"
                  link_text: "En savoir plus"
                  href: "https://about.gitlab.com/analysts/gartner-ast22/"
                  image: /nuxt-images/blogimages/fjct_cover.jpg
                  data_ga_name: "GitLab a challenger in 2022 Gartner Magic Quadrant"
                  data_ga_location: resource cards
    - name: 'report-cta'
      data:
        layout: "dark-shortened"
        title: Rapports d'analystes
        reports:
        - description: "GitLab reconnu comme unique leader dans The Forrester Wave™ dédié aux plateformes de livraison de logiciels intégrés, T2\_2023"
          url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
          link_text: Lire le rapport     
        - description: "GitLab recognized as a Leader in the 2023 Gartner® Magic Quadrant™ for DevOps Platforms"
          url: /gartner-magic-quadrant/
          link_text: Lire le rapport
    - name: solutions-cards
      data:
        title: Faites plus avec GitLab
        column_size: 4
        link :
          url: /solutions/
          text: Explorer d'autres solutions
          data_ga_name: all solutions
          data_ga_location: do more with gitlab
        cards:
          - title: Conformité continue du logiciel
            description: Il est facile d'intégrer la sécurité dans votre cycle de vie DevOps avec GitLab.
            icon:
              name: continuous-integration
              alt: Icône d'intégration continue
              variant: marketing
            href: /solutions/compliance/
            cta: En savoir plus
            data_ga_name: compliance
            data_ga_location: do more with gitlab
          - title: Sécurité de la chaîne d'approvisionnement logicielle
            description: Assurez-vous que votre chaîne d'approvisionnement logicielle est sécurisée et conforme.
            icon:
              name: devsecops
              alt: Icône DevSecOps
              variant: marketing
            href: /solutions/supply-chain/
            cta: En savoir plus
            data_ga_name: supply chain
            data_ga_location: do more with gitlab
          - title: Intégration et livraison continues
            description: Mettez en place une livraison de logiciels reproductible et à la demande
            icon:
              name: continuous-delivery
              alt: Livraison continue
              variant: marketing
            href: /solutions/continuous-integration/
            cta: En savoir plus
            data_ga_name: ci
            data_ga_location: do more with gitlab
