---
  title: GitLab pour GitOps
  description: Favorisez la collaboration entre vos équipes en charge de l'infrastructure, de l'exploitation et du développement. Déployez plus fréquemment avec plus d'assurance tout en augmentant la stabilité, la fiabilité et la sécurité de vos environnements logiciels.
  template: industry
  next_step_alt: true
  no_gradient: true
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab pour GitOps
        subtitle: Automatisation et collaboration au niveau de l'infrastructure pour les environnements cloud-native, multicloud et hérités
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Commencer votre essai gratuit
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        video:
          video_purple_background: true
          hide_in_mobile: true
          video_url: https://www.youtube.com/embed/onFpj_wvbLM?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: 'side-navigation-variant'
      slot_enabled: true
      links:
        - title: Qu'est-ce que GitOps?
          href: '#what-is-gitops'
        - title: GitLab pour GitOps
          href: '#gitlab-for-gitops'
        - title: Avantages
          href: '#advantages'
        - title: Capacités
          href: '#capabilities'
        - title: Mise en œuvre
          href: "#enablement"
        - title: Reconnaissance
          href: '#recognition'
        - title: Ressources
          href: '#resources'
      slot_content:
        - name: 'div'
          id: 'what-is-gitops'
          slot_enabled: true
          slot_content:
            - name: by-solution-intro
              data:
                header: Qu'est-ce que GitOps?
                text:
                  description: GitOps est un cadriciel opérationnel qui applique à l'automatisation et à la gestion de l'infrastructure les meilleures pratiques DevSecOps utilisées pour le développement d'applications, telles que le contrôle de la version, la collaboration, la conformité et CI/CD.
                cta:
                  text: En savoir plus sur GitOps
                  url: /topics/gitops/
                  data_ga_name: Learn more about GitOps
                  data_ga_location: body
        - name: 'div'
          id: 'gitlab-for-gitops'
          slot_enabled: true
          slot_content:
            - name: by-solution-intro
              data:
                header: Pourquoi choisir GitLab pour GitOps ?
                text:
                  description: Favorisez la collaboration entre vos équipes en charge de l'infrastructure, de l'exploitation et du développement. Déployez plus fréquemment avec plus d'assurance tout en augmentant la stabilité, la fiabilité et la sécurité de vos environnements logiciels. Utilisez les fonctionnalités de GitLab pour le contrôle de la version, la révision du code et CI/CD dans une seule application pour une expérience simplifiée. Tirez parti de l'intégration étroite de GitLab avec HashiCorp Terraform et Vault, ainsi que des capacités multicloud, pour vous fournir la meilleure plateforme pour automatiser et gérer l'infrastructure.
                cta:
                  text: Voir une démonstration
                  url: /demo/
                  data_ga_name: Watch a demo
                  data_ga_location: body
        - name: 'div'
          id: 'avantages'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Avantages de GitLab
                is_accordion: true
                is_full_width: true
                items:
                  - icon:
                      name: check-circle-alt
                      alt: icône de contrôle
                      variant: marketing
                    header: Une application unique pour SCM, CI/CD et GitOps
                    text: La gestion du code source, les workflows CI/CD et GitOps sont au cœur de l'automatisation et de la gestion de votre infrastructure. En outre, nos fonctionnalités assistées par IA tout au long du cycle de vie DevSecOps peuvent vous aider à gagner en efficacité et à augmenter la fréquence de vos déploiements.
                  - icon:
                      name: terraform
                      alt: icône terraform
                      variant: product
                    header: Intégration étroite à Terraform
                    text: Terraform est devenu la norme du secteur pour l'approvisionnement de l'environnement. GitLab s'associe à HashiCorp pour assurer le bon fonctionnement de vos outils lorsque vous les associez.
                  - icon:
                      name: collaboration-alt-4-thin
                      alt: icône de collaboration
                      variant: marketing
                    header: Approuvée par les plus grandes équipes d'ingénierie du monde
                    text: De Goldman Sachs et Verizon à Ticketmaster et Siemens, de nombreuses grandes entreprises confient leur code à GitLab plutôt qu'à une autre plateforme.
        - name: 'by-solution-value-prop'
          id: capabilities
          data:
            title: Capacités
            cards:
              - title: Contrôle des versions basé sur Git
                description: Utilisez les outils Git dont vous disposez déjà comme interface pour les opérations. Créez des versions de votre infrastructure en tant que code et appliquez une politique de configuration pour créer des environnements reproductibles. En cas d'incidents, revenez à un dernier état de fonctionnement connu pour réduire vos temps de restauration des services.
                icon:
                  name: merge
                  alt: merge Icon
                  variant: marketing
              - title: Revue de code
                description: Améliorez la qualité du code, communiquez les meilleures pratiques et repérez les erreurs avant qu'elles ne soient publiées grâce aux requêtes de fusion qui suivent et résolvent les fils de conversation, appliquent les suggestions en ligne et fonctionnent de manière asynchrone avec les commentaires en ligne et les commentaires généraux sur les fils de conversation.
                icon:
                  name: ai-merge-request
                  alt: icône de requête de fusion
                  variant: marketing
              - title: Branches et environnements protégés
                description: Permettez à tout le monde de contribuer dans des dépôts de code partagés et définissez qui peut déployer dans des environnements dotés d'autorisations uniques pour les branches protégées et non par défaut.
                icon:
                  name: stop
                  alt: icône d'arrêt
                  variant: marketing
              - title: Workflows CI/CD et GitOps
                description: GitLab fournit un CI/CD puissant et évolutif entièrement développé dans la même application que votre planification Agile et la gestion du code source pour une expérience intégrée. GitLab inclut des tests statiques et dynamiques d'infrastructure en tant que code pour aider à détecter les vulnérabilités avant leur mise en production. GitLab intègre Flux pour prendre en charge les workflows GitOps basés sur l'extraction.
                icon:
                  name: continuous-delivery
                  alt: Icône de livraison continue
                  variant: marketing
              - title: Intégration Terraform
                description: GitLab stocke votre fichier de statut Terraform et les modules affichent la sortie du plan Terraform directement dans la requête de fusion.
                icon:
                  name: terraform
                  alt: Icône Terraform
                  variant: product
              - title: Déployez n'importe où
                description: Des conteneurs et des VM aux déploiements de GitLab Bare Metal à tout moment. Optez pour le multicloud avec AWS, Azure, Google Cloud et plus encore.
                icon:
                  name: cloud-server
                  alt: icône cloud
                  variant: marketing
        - name: 'div'
          id: 'enablement'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Prise en charge de GitOps par GitLab
                is_accordion: true
                is_full_width: true
                items:
                  - icon:
                      name: code
                      alt: icône de code
                      variant: marketing
                    header: L'environnement en tant que code
                    text: Stocké dans le contrôle de version GitLab en tant que source unique de référence.
                  - icon:
                      name: idea-collaboration
                      alt: icône d'idée
                      variant: marketing
                    header: Collaboration des équipes
                    text: Utilisation de la planification Agile et de la revue de code de GitLab.
                  - icon:
                      name: devsecops
                      alt: Icône DevSecOps
                      variant: marketing
                    header: Un outil
                    text: Utilisé pour planifier, créer des versions et déployer le code de votre application, qui fonctionne également pour le code de vos opérations.
                  - icon:
                      name: microservices-cog
                      alt: icône d'un rouage
                      variant: marketing
                    header: Workflows CI/CD et GitOps pour l'automatisation et la gestion de l'infrastructure
                    text: Réconcilie vos environnements avec votre source unique de référence dans le contrôle de version.
        - name: 'report-cta'
          id: 'recognition'
          slot_enabled: true
          data:
            layout: "dark"
            title: Rapports d'analystes
            reports:
            - description: "GitLab reconnu comme unique leader dans The Forrester Wave™ dédié aux plateformes de livraison de logiciels intégrés, T2\_2023"
              url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
            - description: "GitLab recognized as a Leader in the 2023 Gartner® Magic Quadrant™ for DevOps Platforms"
              url: /gartner-magic-quadrant/
        
      
        - name: 'solutions-resource-carousel'
          slot_enabled: true
          id: resources
          data:
            column_size: 4
            title: Ressources complémentaires
            carousel: true
            cards:
              - icon:
                  name: video
                  alt: Icône de vidéo
                  variant: marketing
                event_type: Vidéo
                header: Qu'est-ce que GitOps? Comment démarrer?
                link_text: Regarder maintenant
                fallback_image: /nuxt-images/resources/fallback/img-fallback-cards-devops.png
                href: https://www.youtube.com/watch?list=PL05JrBw4t0KoixDjLVMzKwVgx0eNyDOIo&v=JtZfnrwOOAw
              - icon:
                  name: web
                  alt: icône web
                  variant: marketing
                event_type: Web
                header: Qu'est-ce que GitOps?
                link_text: En savoir plus sur GitOps
                fallback_image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
                href: /topics/gitops/
                data_ga_name: "GitOps: The Future of Infrastructure Automation"
                data_ga_location: resource cards
              - icon:
                  name: web
                  alt: icône web
                  variant: marketing
                event_type: Web
                header: Pourquoi la technologie collaborative est essentielle pour GitOps
                link_text: En savoir plus sur GitOps
                fallback_image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
                href: /topics/gitops/gitops-gitlab-collaboration/
                data_ga_name: "Why collaboration technology is critical for GitOps"
                data_ga_location: resource cards
              - icon:
                  name: blog
                  alt: icône de blog
                  variant: marketing
                event_type: Blog
                header: Comment utiliser GitLab et Ansible pour créer une infrastructure en tant que code
                link_text: Lire l'article de blog
                fallback_image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
                href: /blog/2019/07/01/using-ansible-and-gitlab-as-infrastructure-for-code/
                data_ga_name: How to use GitLab and Ansible to create infrastructure as code
                data_ga_location: resource cards
              - icon:
                  name: article
                  alt: icône d'article
                  variant: marketing
                event_type: Article
                header: "GitOps: l'avenir de l'automatisation des infrastructures"
                link_text: En savoir plus sur GitOps
                image: /nuxt-images/features/resources/resources_webcast.png
                href: /why/gitops-infrastructure-automation/
                data_ga_name: "GitOps: The Future of Infrastructure Automation"
                data_ga_location: resource cards
