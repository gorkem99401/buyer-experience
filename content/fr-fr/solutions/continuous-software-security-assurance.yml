---
  title: Sécurité logicielle continue
  description: Intégrer la sécurité dans votre cycle de vie DevSecOps, c'est facile avec GitLab. Sécurité et conformité sont intégrées, prêtes à l'emploi, ce qui vous offre la visibilité et le contrôle nécessaires pour protéger l'intégrité de votre logiciel.
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: Sécurité logicielle continue
        subtitle: La sécurité dans l'ensemble du cycle de vie avec le DevSecOps intégré
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Commencer votre essai gratuit
          url: /free-trial/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image\_: boucle infinie GitLab"
    - name: 'side-navigation-variant'
      slot_enabled: true
      links:
        - title: Présentation
          href: '#overview'
        - title: Avantages
          href: '#benefits'
        - title: Capacités
          href: '#capabilities'
        - title: Tarifs
          href: "#pricing"
        - title: Études de cas
          href: '#case-studies'
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: by-solution-intro
              data:
                text:
                  highlight: Intégrer la sécurité dans votre cycle de vie DevSecOps, c'est facile avec GitLab.
                  description: Sécurité et conformité sont intégrées, prêtes à l'emploi, ce qui vous offre la visibilité et le contrôle nécessaires pour protéger l'intégrité de votre logiciel.
            - name: by-solution-benefits
              data:
                title: Sécurité. Conformité. Intégration.
                image:
                  image_url: "/nuxt-images/resources/resources_11.jpeg"
                  alt: travail sur des ordinateurs portables
                is_accordion: true
                items:
                  - icon:
                      name: devsecops
                      alt: Icône DevSecOps
                      variant: marketing
                    header: Tests et remédiation intégrés
                    text: Avec chaque livraison de code, GitLab fournit [aux développeurs des résultats exploitables](https://docs.gitlab.com/ee/user/application_security/) en matière de sécurité et de conformité, afin de prendre des mesures correctives en amont dans le cycle de vie, pendant qu'ils travaillent encore sur le code.
                  - icon:
                      name: continuous-integration
                      alt: Icône d'intégration continue
                      variant: marketing
                    header: Gestion des vulnérabilités des logiciels
                    text: Tout en aidant les professionnels de la sécurité à [gérer les vulnérabilités restantes](https://docs.gitlab.com/ee/user/application_security/security_dashboard/#gitlab-security-dashboards-and-security-center) grâce à la résolution.
                  - icon:
                      name: cloud-tick
                      alt: Icône d'intégration continue
                      variant: marketing
                    header: Sécurité des applications cloud-native
                    text: GitLab vous aide à sécuriser vos applications cloud-native et l'infrastructure dont elles dépendent, y compris les conteneurs, l'Infrastructure as Code et les API.
                  - icon:
                      name: automated-code
                      alt: Icône de code automatisé
                      variant: marketing
                    header: Garde-fous et automatisation des politiques
                    text: Les pipelines de conformité de GitLab, les approbations des requêtes de fusion, la transparence de bout en bout des événements d'audit, ainsi que les [contrôles communs](https://docs.gitlab.com/ee/administration/compliance.html) intégrés vous aident à sécuriser votre chaîne d'approvisionnement logicielle et à répondre à vos [besoins en matière de conformité](https://about.gitlab.com/solutions/compliance/).
        - name: 'by-solution-value-prop'
          id: benefits
          data:
            title: Rapidité et sécurité pour vos développeurs
            cards:
              - title: Simplicité
                description: Une plateforme, un prix, et une sécurité complète des applications.
                list:
                  - text: <a href="https://docs.gitlab.com/ee/user/application_security/" data-ga-name="application security testing" data-ga-location="body">Tests de sécurité des applications</a>
                  - text: <a href="https://docs.gitlab.com/ee/user/application_security/vulnerability_report/" data-ga-name="vulnerability management" data-ga-location="body">Gestion des vulnérabilités</a>
                  - text: <a href="https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/" data-ga-name="deployed images" data-ga-location="body">Scan des images déployées</a>
                icon:
                  name: release
                  alt: Icône Agile
                  variant: marketing
              - title: Visibilité
                description: Vérifiez qui a changé quoi, où, et quand, de bout en bout.
                list:
                  - text: <a href="https://docs.gitlab.com/ee/administration/audit_events.html" data-ga-name="audit events" data-ga-location="body">Événements d'audit</a>
                  - text: <a href="https://docs.gitlab.com/ee/administration/audit_reports.html" data-ga-name="audit reports" data-ga-location="body">Rapports d'audit</a>
                  - text: <a href="https://docs.gitlab.com/ee/user/application_security/dependency_list/#dependency-list" data-ga-name="dependency list" data-ga-location="body">Liste des dépendances (BOM)</a>
                icon:
                  name: magnifying-glass-code
                  alt: Icône Code loupe
                  variant: marketing
              - title: Contrôle
                description: Un cadre de conformité pour la cohérence, les contrôles communs, et l'automatisation des politiques.
                list:
                  - text: <a href="https://docs.gitlab.com/ee/administration/compliance.html" data-ga-name="compliance capabilites" data-ga-location="body">Contrôles de conformité communs</a>
                  - text: <a href="https://docs.gitlab.com/ee/user/application_security/configuration/" data-ga-name="security policy configuration" data-ga-location="body">Configuration de la politique de sécurité</a>
                  - text: <a href="https://docs.gitlab.com/ee/user/project/settings/index.html#compliance-pipeline-configuration" data-ga-name="compliant pipelines" data-ga-location="body">Pipelines de conformité</a>
                icon:
                  name: less-risk
                  alt: Icône Moins de risque
                  variant: marketing
        - name: 'home-solutions-container'
          id: capabilities
          data:
            title: Sécurité de la plateforme DevSecOps
            image: /nuxt-images/home/solutions/solutions-top-down.png
            alt: "Image en plongée d'un bureau"
            description: Rendez-vous dans notre Trust Center pour découvrir les mesures que nous mettons en place pour sécuriser le [logiciel GitLab](https://about.gitlab.com/security/) et répondre aux normes du secteur.
            white_bg: true
            animation_type: fade
            solutions:
              - title: Testez dans le pipeline CI
                description: Utilisez vos scanners ou les nôtres. Intégrez la sécurité en amont du cycle de développement pour permettre aux développeurs de trouver et de corriger les failles de sécurité à mesure qu'elles apparaissent. Les scanners complets incluent SAST, DAST, secrets, dépendances, conteneurs, IaC, API, images de cluster et tests à données aléatoires.
                link_text: En savoir plus
                link_url: https://docs.gitlab.com/ee/user/application_security/
                data_ga_name: Test CI pipeline
                data_ga_location: body
                icon:
                  name: pipeline-alt
                  alt: Icône de pipeline
                  variant: marketing
                image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_1.png
                alt: "Bulles de texte d'équipes qui communiquent"
              - title: Évaluez les dépendances
                description: Scannez les dépendances et les conteneurs à la recherche de failles de sécurité. Faites l'inventaire des dépendances utilisées.
                icon:
                  name: visibility
                  alt: Icône visibilité
                  variant: marketing
                image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_2.png
                alt: "Bulles de texte d'équipes qui communiquent"
              - title: Sécurisez les applis cloud-native
                description: Testez la sécurité des éléments cloud-native, comme l'Infrastructure as Code, les API et les images de cluster.
                icon:
                  name: cloud-tick
                  alt: Icône d'un nuage avec une coche
                  variant: marketing
                image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_3.png
                alt: "Bulles de texte d'équipes qui communiquent"
              - title: Gérez les vulnérabilités
                description: Conçu pour les professionnels de la sécurité pour examiner, trier et gérer les vulnérabilités logicielles des pipelines, des analyses à la demande, des tiers et des primes aux bogues en un seul et même endroit. Visibilité immédiate à mesure que les vulnérabilités sont fusionnées. Collaborez plus facilement à leur résolution.
                icon:
                  name: continuous-integration
                  alt: Icône d'intégration continue
                  variant: marketing
                image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_4.png
                alt: "Bulles de texte d'équipes qui communiquent"
              - title: Sécurisez votre chaîne d'approvisionnement logicielle
                description: Automatisez les politiques de sécurité et de conformité tout au long de votre cycle de développement logiciel. Les pipelines de conformité garantissent que les politiques de pipeline ne sont pas contournées, et les contrôles communs fournissent des garde-fous de bout en bout.
                icon:
                  name: shield-check
                  alt: Icône de bouclier avec une coche
                  variant: marketing
                image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_5.png
                alt: "Bulles de texte d'équipes qui communiquent"
        - name: 'tier-block'
          class: 'slp-mt-96'
          id: 'pricing'
          data:
            header: Quelle édition choisir ?
            tiers:
              - id: free
                title: Gratuit
                items:
                  - Tests de sécurité statique des applications (SAST) et détection des secrets
                  - Résultats d'analyses en format json
                link:
                  href: /pricing/
                  text: Commencer
                  data_ga_name: pricing
                  data_ga_location: free tier
                  aria_label: free tier
              - id: premium
                title: Premium
                items:
                  - Tests de sécurité statique des applications (SAST) et détection des secrets
                  - Résultats d'analyses en format json
                  - Approbations des requêtes de fusion et plus de contrôles communs
                link:
                  href: /pricing/
                  text: En savoir plus
                  data_ga_name: pricing
                  data_ga_location: premium tier
                  aria_label: premium tier
              - id: ultimate
                title: Ultimate
                items:
                  - Tous les avantages de l'édition Premium, et...
                  - Scanners de sécurité complets (SAST, DAST, secrets, dépendances, conteneurs, IaC, API, images de cluster et tests à données aléatoires)
                  - Résultats exploitables dans le pipeline MR
                  - Pipelines de conformité
                  - Tableaux de bord Sécurité et Conformité
                  - Et bien plus encore
                link:
                  href: /pricing/
                  text: En savoir plus
                  data_ga_name: pricing
                  data_ga_location: ultimate tier
                  aria_label: ultimate tier
                cta:
                  href: /free-trial/
                  text: Essayer Ultimate gratuitement
                  data_ga_name: pricing
                  data_ga_location: ultimate tier
                  aria_label: ultimate tier
        - name: 'by-industry-case-studies'
          id: case-studies
          data:
            title: Témoignages de clients
            link:
              text: Toutes les études de cas
            charcoal_bg: true
            rows:
              - title: HackerOne
                subtitle: HackerOne réalise des déploiements 5  fois plus rapides avec la sécurité intégrée de GitLab
                image:
                  url: nuxt-images/blogimages/hackerone-cover-photo.jpg
                  alt: Ordinateur avec du code
                button:
                  href: /customers/hackerone/
                  text: En savoir plus
                  data_ga_name: learn more
                  data_ga_location: body
              - title: The Zebra
                subtitle: Découvrez comment The Zebra a réussi à sécuriser ses pipelines
                image:
                  url: nuxt-images/blogimages/thezebra_cover.jpg
                  alt: Téléphone portable prenant une photo d'une voiture
                button:
                  href: /customers/thezebra/
                  text: En savoir plus
                  data_ga_name: learn more
                  data_ga_location: body
              - title: Hilti
                subtitle: Découvrez comment Hilti a accéléré son SDLC grâce à la CI/CD et une analyse fiable de sécurité
                image:
                  url: nuxt-images/blogimages/hilti_cover_image.jpg
                  alt: Gratte-ciel en construction
                button:
                  href: /customers/hilti/
                  text: En savoir plus
                  data_ga_name: learn more
                  data_ga_location: body

    - name: group-buttons
      data:
        header:
          text: GitLab contribue de nombreuses manières à la sécurité logicielle continue. Découvrez comment.
          link:
            text: Explorer plus de solutions
            href: /solutions/
        buttons:
          - text: Automatisation de la livraison
            icon_left: automated-code
            href: /solutions/delivery-automation/
          - text: Intégration continue
            icon_left: continuous-delivery
            href: /solutions/continuous-integration/
          - text: Conformité
            icon_left: shield-check
            href: /solutions/compliance/
    - name: 'solutions-resource-cards'
      data:
        column_size: 4
        title: Ressources
        link:
          text: Voir toutes les ressources
        cards:
          - icon:
              name: webcast
              alt: Icône Webcast
              variant: marketing
            event_type: Vidéo
            header: Démo de présentation de DevSecOps
            link_text: Regarder maintenant
            fallback_image: /nuxt-images/resources/fallback/img-fallback-cards-devops.png
            href: https://youtu.be/2mmw3SF7Few
            aos_animation: fade-up
            aos_duration: 400
          - icon:
              name: webcast
              alt: Icône Webcast
              variant: marketing
            event_type: Vidéo
            header: Découvrez comment intégrer la sécurité à votre pipeline CI/CD
            link_text: Regarder maintenant
            image: /nuxt-images/features/resources/resources_webcast.png
            href: https://youtu.be/Fd5DhebtScg
            aos_animation: fade-up
            aos_duration: 600
          - icon:
              name: webcast
              alt: Icône Webcast
              variant: marketing
            event_type: Vidéo
            header: Gérez efficacement les vulnérabilités et les risques à l'aide des tableaux de bord de sécurité GitLab
            link_text: Regarder maintenant
            fallback_image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
            href: https://youtu.be/p3qt2z1rQk8
            aos_animation: fade-up
            aos_duration: 800
          - icon:
              name: webcast
              alt: Icône Webcast
              variant: marketing
            event_type: Vidéo
            header: Gérez les dépendances de vos applications
            link_text: Regarder maintenant
            image: /nuxt-images/features/resources/resources_waves.png
            href: https://youtu.be/scNS4UuPvLI
            aos_animation: fade-up
            aos_duration: 1000
