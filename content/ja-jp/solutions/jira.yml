---
  title: GitLabとJira
  description: GitLabからJiraまで作業を自動化しましょう
  components:
    - name: 'solutions-hero'
      data:
        title: GitLabとJira
        subtitle: GitLabからJiraまで作業を自動化しましょう
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: /learn/#agile_management
          text: 学習を開始
          data_ga_name: start learning
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "画像: GitLabとJira"
    - name: featured-media
      data:
        column_size: 4
        media:
          - title: GitLab<->Jiraインテグレーション
            aos_animation: fade-up
            aos_duration: 500
            text: |
              GitLabプロジェクトをJiraインスタンスと統合すると、GitLabプロジェクトとJira内の任意のプロジェクト間のアクティビティを自動的に検出し、相互参照できるようになります。
            icon:
              name: kanban
              alt: かんばん
              variant: marketing
          - title: Jira開発パネルのインテグレーション
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              既存のJiraプロジェクトのインテグレーションを補完できるよう、GitLabプロジェクトをJira開発パネルと統合できるようになりました。
            icon:
              name: computer-test
              alt: Jira開発パネルのインテグレーション
              variant: marketing
          - title: JiraからGitLabへの移行
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              GitLabには、堅牢なプロジェクト管理ツールが含まれるため、SCM、CI/CD、セキュリティなどを行える単一のプラットフォーム上にアジャイル計画を統合できます。
            icon:
              name: scale
              alt: Jira開発パネルのインテグレーション
              variant: marketing
    - name: featured-media
      data:
        header: GitLab-Jiraインテグレーションの仕組み
        column_size: 4
        media:
          - title: GitLab<->Jira基本インテグレーション
            aos_animation: zoom-in-up
            aos_duration: 500
            text: |
              *コミットメッセージまたはMR（マージリクエスト）でJiraイシューIDをメンションします。
              
              *コミットまたはMRによって特定のJiraイシューが解決または完了することをメンションします。
              
              *GitLabでJiraイシューを直接表示します。
            image:
              url: /nuxt-images/enterprise/source-code.jpg
              alt: "GitLab<->Jira"
          - title: Jira開発パネルのインテグレーション
            aos_animation: zoom-in-up
            aos_duration: 1000
            text: |
              *GitLabの関連するマージリクエスト、ブランチ、およびコミットにJiraイシューから直接簡単にアクセスできます。
              
              *Jira Cloud上のJiraと統合されたGitLab Self-ManagedまたはGitLab.comで動作します。
              
              *トップレベルグループまたは個人の名前空間内のすべてのGitLabプロジェクトをJiraインスタンスのプロジェクトに接続します。
            image:
              url: /nuxt-images/enterprise/ci-cd.jpg
              alt: "パネルのインテグレーション"
          - title: JiraからGitLabへの移行
            aos_animation: zoom-in-up
            aos_duration: 1500
            text: |
              *JiraイシューをGitLab.comまたはGitLab Self-Managedインスタンスにインポートします。
              
              *タイトル、説明、ラベルを直接インポートします。
              
              *JiraユーザーをGitLabプロジェクトメンバーにマップします。
            image:
              url: /nuxt-images/enterprise/agile2.jpg
              alt: "GitLabからJiraへの移行"
    - name: featured-media
      data:
        header: 使い方に関する動画
        text: |
          GitLab-Jiraインテグレーションに関するリソースで、実装について理解する際に特に役立ちそうなものをご紹介します。
        column_size: 4
        media:
          - title: GitLab-Jiraの基本的なインテグレーション
            aos_animation: fade-up
            aos_duration: 500
            text: |
              JiraからGitLabのイシューへのコンテンツとプロセスの移行はいつでも行えますが、ご希望の場合はGitLabと一緒にJiraを引き続き使用できます。
            link:
              text: 読む
              href: https://docs.gitlab.com/ee/integration/jira/
              data_ga_name: Gitlab-Jira integration
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/fWvwkx5_00E?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: GitLab-Jira開発パネル
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              GitLabプロジェクトをJira開発パネルと統合して、既存のJiraプロジェクトのインテグレーションを補完することができます。
            link:
              text: 読む
              href: https://docs.gitlab.com/ee/integration/jira/development_panel.html
              data_ga_name: Gitlab-Jira development panel
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/VjVTOmMl85M?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: JiraプロジェクトのイシューをGitLabにインポート
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              GitLab Jiraインポート機能を使用すると、JiraイシューをGitLab.comまたはGitLabのSelf-Managedインスタンスにインポートできます。
            link:
              text: 読む
              href: https://docs.gitlab.com/ee/user/project/import/jira/
              data_ga_name: Import jira issue to GitLab
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/cixs4NJB3H8?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: JiraイシューのリストをGitLabで表示
            aos_animation: fade-up
            aos_duration: 500
            text: |
              Jiraを主な作業追跡ツールとして使用している組織では、貢献者が複数のシステムにまたがって作業する場合、単一の信頼できる情報源を保守することは難しい場合があります。
            link:
              text: 読む
              href: https://docs.gitlab.com/ee/user/project/import/jira/
              data_ga_name: View jira issue list in GitLab
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/_yDcD_jzSjs?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: イシューのインポート時にJiraユーザーをGitLabユーザーにマップ
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              JiraからGitLabにイシューをインポートする際、インポートの実行前にJiraユーザーをGitLabプロジェクトメンバーにマップできるようになりました。これにより、インポートを行うユーザーは、GitLabに移動するイシューに対して適切なレポーターと担当者を設定できます。
            link:
              text: 読む
              href: https://docs.gitlab.com/ee/user/project/import/jira/
              data_ga_name: Map jira users to GitLab
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/cixs4NJB3H8?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: GitLabプロジェクト管理ロードマップ
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              GitLabでは、Jiraとのインテグレーションの改善に常に取り組んでいます。今後のリリースで予定されている内容をぜひご確認ください。また、皆様からのフィードバックをお待ちしています。
            link:
              text: 読む
              href: https://gitlab.com/groups/gitlab-org/-/epics/2738/
              data_ga_name: GitLab project management roadmap
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/bT60rJEoWhw?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: logo-links
      data:
        header: オープンソースパートナー
        column_size: 2
        logos:
          - logo_url: /nuxt-images/enterprise/logo-dish.svg
            logo_alt: Dish
            aos_animation: zoom-in
            aos_duration: 200
          - logo_url: /nuxt-images/enterprise/logo-expedia.svg
            logo_alt: Expedia
            aos_animation: zoom-in
            aos_duration: 400
          - logo_url: /nuxt-images/enterprise/logo-goldman-sachs.svg
            logo_alt: Goldman Sachs
            aos_animation: zoom-in
            aos_duration: 600
          - logo_url: /nuxt-images/enterprise/logo-nasdaq.svg
            logo_alt: Nasdaq
            aos_animation: zoom-in
            aos_duration: 800
          - logo_url: /nuxt-images/enterprise/logo-uber.svg
            logo_alt: Uber
            aos_animation: zoom-in
            aos_duration: 1000
          - logo_url: /nuxt-images/enterprise/logo-verizon.svg
            logo_alt: Verizon
            aos_animation: zoom-in
            aos_duration: 1200