/* eslint @typescript-eslint/no-var-requires: 0 */
/* eslint no-console: 0 */
const yaml = require('js-yaml');
const fs = require('fs');
const contentful = require('contentful-management');
const process = require('process');
const newsFile =
  '/Users/mateofernandopenagos/Documents/gitlab/www-gitlab-com/data/press.yml'; // news file in www to use as migration target
const accessToken = process.argv[2]; // CMA access token
const spaceId = process.argv[3]; // About.gitlab.com space id

async function migrateRecentNews(auth) {
  const client = contentful.createClient({
    accessToken: auth.accessToken,
  });
  const environment = await client
    .getSpace(auth.spaceId)
    .then((space) => space.getEnvironment(auth.env));
  const parsedNews = readNewsFile(newsFile);
  const landingCardIds = [];

  // const testEntry = [ parsedNews[50]]
  for (const [index, post] of parsedNews.entries()) {
    //  Create required entries for the press release post:
    //  Create landing card entries
    const cardId = await createLandingCardEntry(post, environment);
    landingCardIds.push(cardId);
    console.log(`PROGRESS: ${Math.round((index / parsedNews.length) * 100)}%`);
  }

  await LinkToParent(landingCardIds, environment);
}

function readNewsFile(path) {
  console.log('parsing news file to JSON');
  const yml = fs.readFileSync(path, { encoding: 'utf8', flag: 'r' });
  const parsedYML = yaml.load(yml, { json: true });
  console.log(`parsed ${parsedYML.recent_news.length} news files to JSON`);
  return parsedYML.recent_news.filter(
    (news) => news.date.getFullYear() === 2023,
  );
}

async function createLandingCardEntry(card, environment) {
  const contentType = 'landingGridCard';
  console.log(`creating Landing grid card for ${card.title}`);
  try {
    const textBlock = await environment.createEntry(contentType, {
      fields: {
        internalName: {
          'en-US': card.title,
        },
        header: {
          'en-US': card.title,
        },
        date: {
          'en-US': card.date.toISOString().split('T')[0],
        },
        slug: {
          'en-US': card.link,
        },
      },
    });
    console.log('Created card');
    return textBlock.sys.id;
  } catch ({ name, message }) {
    const parsedError = JSON.parse(message);
    console.log(parsedError.message);
  }
}

async function LinkToParent(landingCardIds, environment) {
  const parent = await environment.getEntry('4UnndGveTxeJOKBxGbACnp');

  const linkedEntries = landingCardIds.map((id) => ({
    sys: {
      type: 'Link',
      linkType: 'Entry',
      id,
    },
  }));

  parent.fields.cards = {
    'en-US': linkedEntries,
  };

  try {
    await parent.update();
    // await updatedparent.publish();
    console.log(`Entry updated`);
  } catch (error) {
    console.error(`Error Updating Entry ${error}`);
  }
}

migrateRecentNews({
  accessToken,
  spaceId,
  env: 'master',
});
