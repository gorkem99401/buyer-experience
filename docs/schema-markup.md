# Structured data Schema markups

Webpages should include `json` schemas of the structured data they display in order to help crawlers and bots improve SEO rankings for the page. They also allows rich results to be provided en search results.

Please visit [schema.org](https://schema.org/) for more details.

## Schemas

We defined a couple of schema markups inside the Buyer Experience repository. Utility function `getPageMetadata` in the `/common/meta.ts` file automatically generates schemas for content consumed in pages in this repository.

Firstly, it adds basic metadata such as title, description and social media thumbnails.

At the top of the file, we import some helper functions from `/common/craftSchema.ts`.

``` js
import { createSchemaPage, createSchemaFaq, createSchemaBreadcrumb } from './craftSchema';
```

### Faq

For the Faq component, we add a script `.push` in order for the json structured data to be injected into the HTML document. We use the `createSchemaPage` function to generate the data.

``` js
  if (schemaFaq) {
    script.push({
      hid: 'schemaFaq',
      json: createSchemaFaq(schemaFaq),
      type: 'application/ld+json',
    });
  }
```

The example data for this type of structured data is as follows:

``` json
<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "FAQPage",
      "mainEntity": [
        {
          "@type": "Question",
          "name": "<p>What is Agile delivery?</p>
",
          "acceptedAnswer": {
            "@type": "Answer",
            "text": "
            <p>Agile delivery is an iterative approach to software delivery in which teams build software  incrementally at the beginning of a project rather than ship it at once upon completion.</p>

                <a href='https://about.gitlab.com/topics/agile-delivery/#what-is-agile-delivery:~:text=What%20is%20Agile%20delivery%3F'>Learn more about Agile delivery</a>
            "
          }
        }, 
        {
          "@type": "Question",
          "name": "<p>Why embrace Agile delivery?</p>
",
          "acceptedAnswer": {
            "@type": "Answer",
            "text": "
            <p>Businesses that empower teams to use Agile development practices satisfy discerning customers and adapt to new technologies, helping them to develop the products that set the standard for industries.</p>

                <a href='https://about.gitlab.com/topics/agile-delivery/#why-embrace-agile-delivery:~:text=Why%20embrace%20Agile%20delivery'>Learn more about embracing Agile delivery</a>
            "
          }
        }  
      ] 
    }
  </script>
```

### WebPage

For a WebPage, we first check the url path in order to match the pages we want to target.
Make sure to specify the target url path of pages you want a WebPage schema to be added with: `path.includes('<your-page-path>')`

Then we add a script `.push` in order for the `json` structured data to be injected into the HTML document.

``` js
  if (path.includes('topics')) {
    script.push(
      buildTopicsSchema({
        title,
        description,
        path,
        topicName,
        topicsHeader,
        dateModified,
        datePublished,
      }),
    );
  }
```

The `buildTopicsSchema` function defined in the `/common/meta.ts` file would then make use of the WebPage generator function `createSchemaPage`.

``` js
function buildTopicsSchema({
  title,
  description,
  path,
  topicName,
  topicsHeader,
  dateModified,
  datePublished,
}: {
  title: string;
  description: string;
  path: string;
  datePublished?: string;
  dateModified?: string;
  topicName?: string;
  topicsHeader?: any;
}) {
  const schemaValues = {
    title,
    description,
    datePublished,
    dateModified: dateModified || datePublished,
    url: `${SITE_URL}${path}`,
    articleSection: topicName,
    timeRequired: topicsHeader?.data.read_time || '',
    image: '/nuxt-images/topics/gitops-topic.png',
  };

  return {
    hid: 'schemaTopics',
    json: createSchemaPage(schemaValues),
    type: 'application/ld+json',
  };
}
```

The example data for this type of structured data is as follows:

``` json
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Article",
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "https://about.gitlab.com/topics/devsecops/"
  },
  "headline": "What is DevSecOps?",
  "description": "Effectively turn your DevOps methodology into a DevSecOps methodology",
  "image": "https://about.gitlab.com/_nuxt/image/da4ee5.png",
  "articleSection":"DevSecOps",
  "timeRequired":"PT05M",
  "author": {
    "@type": "Organization",
    "name": "GitLab",
    "url": "https://about.gitlab.com/"
  },  
  "publisher": {
    "@type": "Organization",
    "name": "GitLab",
    "logo": {
      "@type": "ImageObject",
      "url": "https://about.gitlab.com/images/press/logo/png/gitlab-logo-500.png"
    }
  },
  "datePublished": "2023-03-01",
  "dateModified": "2023-03-01"
}
</script>
```

### Breadcrumbs

For a Breadcrumbs component, we use the helper function directly in the component. We first import the `createSchemaBreadcrumb` helper function from the `/common/craftSchema.ts` file.

``` js
import { createSchemaBreadcrumb } from '~/common/craftSchema';
```

We then use the `beforeMount` and  `head` nuxt built-in methods to load and inject the json structured data for Breadcrumbs.

``` js
export default Vue.extend({
  name: 'Breadcrumb',
  props: {
    crumbs: {
      type: Array as PropType<Breadcrumb[]>,
      required: true,
      default: () => [],
    },
  },
  data() {
    return {
      structuredData: {},
    };
  },
  head() {
    return {
      // Insert script for breadcrumb schema
      script: [
        {
          type: 'application/ld+json',
          json: this.structuredData,
        },
      ],
    };
  },
  beforeMount() {
    // Replace schema values with loaded crumbs
    this.structuredData = createSchemaBreadcrumb(this.$props.crumbs);
  },
});
</script>

```

The example data for this type of structured data is as follows:

``` json
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Topics",
    "item": "https://about.gitlab.com/topics/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "GitOps",
    "item": "https://about.gitlab.com/topics/gitops/"  
  },{
    "@type": "ListItem", 
    "position": 3, 
    "name": "What is a GitOps workflow?",
    "item": "https://about.gitlab.com/topics/gitops/gitops-workflow/"  
  }]
}
</script>
```
